import json

# Import configuration
import ConfigParser
configparser = ConfigParser.RawConfigParser()
configparser.read('mailer.conf')

config = {}

try: 
    config = {
        # Mailer configs
        'mail server': {
            'server': configparser.get('Mailing Server', 'server') if configparser.has_option('Mailing Server', 'server') else 'localhost',
            'port': configparser.get('Mailing Server', 'port') if configparser.has_option('Mailing Server', 'port') else 9200,
            'username': configparser.get('Mailing Server', 'username') if configparser.has_option('Mailing Server', 'username') else None,
            'password': configparser.get('Mailing Server', 'password') if configparser.has_option('Mailing Server', 'password') else None,
        },
        # ElasticSearch configs
        'elasticsearch': {
            'hosts': json.loads(configparser.get('ElasticSearch', 'hosts')),
            'query': json.loads(configparser.get('ElasticSearch', 'query')),
            'index': configparser.get('ElasticSearch', 'index'),
        },
        # Field selection configs
        'fields': {
            'split by': json.loads(configparser.get('Fields', 'split_fields')),
            'internal': json.loads(configparser.get('Fields', 'internal_fields')),
            'fields': json.loads(configparser.get('Fields', 'fields')),
            'to report': json.loads(configparser.get('Fields', 'internal_fields')) + json.loads(configparser.get('Fields', 'fields')),
        }
    }
except Exception, e:
    import sys
    sys.stderr.write('Error while importing config file: %r\n' % str(e))
    sys.exit(-1)