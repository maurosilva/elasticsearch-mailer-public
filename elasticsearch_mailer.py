# Imports
from elasticsearch import Elasticsearch
from datetime import datetime
import argparse

# Local imports
import packaging # Meant to return the attachments of the e-mail
import templating # Meant to return the text of the e-mail
import addressing # Meant to return the subject, from, to, cc and reply-to headers
import configloader # Loads the config for the mailer

#import utils
import email_utils
import elasticsearch_utils

# Constants
index_size = 1000000

# ------------------------------------
# Arguments parser config
# ------------------------------------
date_format = '%d-%m-%Y %H:%M:%S.%f'
date_format_string = 'dd-mm-yyyy hh:mm:ss.uuuuuu'

parser = argparse.ArgumentParser(description='ElasticSearch Mailer')
parser.add_argument('--from-timestamp', dest='from_timestamp', metavar=date_format_string, help='Timestamp to begin the report on (inclusive)')
parser.add_argument('--to-timestamp', dest='to_timestamp', metavar=date_format_string, help='Timestamp to end the report on (exclusive)')
args = parser.parse_args()

from_timestamp = None
to_timestamp = None
try:
    from_timestamp = datetime.strptime(args.from_timestamp, date_format)
    to_timestamp = datetime.strptime(args.to_timestamp, date_format)
except Exception, e:
    import sys
    if from_timestamp is None:
        sys.stderr.write('Error when parsing from_timestamp: %r\n' % str(e))
    elif to_timestamp is None:
        sys.stderr.write('Error when parsing to_timestamp: %r\n' % str(e))
    else:
        sys.stderr.write('Error: %r\n' % str(e))
    sys.exit(-1)
    
# ------------------------------------
# Main Script
# ------------------------------------
config = configloader.config
es = Elasticsearch(config['elasticsearch']['hosts'])

result = es.indices.stats(index=config['elasticsearch']['index'], metric='docs')
index_size = result['_all']['total']['docs']['count']

# Query ElasticSearch for split field values
body = elasticsearch_utils.create_aggregation_query(config['elasticsearch']['query'], index_size, config['fields']['split by'], from_timestamp, to_timestamp)

# TODO: METER COMMENT
result = es.search(index=config['elasticsearch']['index'], body=body, fields=config['fields']['fields'])
aggregation_values = elasticsearch_utils.parse_aggregation_result(config['fields']['split by'], result)

# Query ElasticSearch for events for every bucket
for combination in aggregation_values:
    specific_body = elasticsearch_utils.create_bucket_query(config['elasticsearch']['query'], index_size, config['fields']['split by'], combination, from_timestamp, to_timestamp)
    result = es.search(index=config['elasticsearch']['index'], body=specific_body, fields=config['fields']['fields'])
    dict_result = elasticsearch_utils.parse_bucket_result(result, config['fields']['internal'])

    # Meant to return the subject, from, to, cc and reply-to headers    
    headers = addressing.get_headers(configloader, config['fields']['fields'], combination)
    # Meant to return the text and attachments of the e-mail
    text = templating.get_body(configloader, config['fields']['to report'], dict_result, combination)
    attachments = packaging.get_attachments(configloader, config['fields']['to report'], dict_result, combination)
    
    headers['text'] = text
    headers['attachments'] = attachments
    mail_to_send = headers
    email_utils.send_mail(configloader, mail_to_send)

