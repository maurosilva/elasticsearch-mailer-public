# Helper functions
def get_single_value(field):
    if is_iterable(field):
        return field[0]
    else:
        return field
    
def is_iterable(obj):
    return hasattr(obj, '__iter__')

# Converts the result of an ElasticSearch query into a list of dictionaries which represent events
def parse_bucket_result(es_search, internal_fields):
    event_list = list()
    if 'hits' in es_search:
        es_search_hits = es_search['hits']
        if 'hits' in es_search_hits:
            es_search_hits = es_search['hits']['hits']
            for hit in es_search_hits:
                event = dict()
                # For each hit gather internal fields into the event
                for ifield in internal_fields:
                    if ifield in hit:
                        event[ifield] = get_single_value(hit[ifield])
                        
                for (field, value) in hit['fields'].items():
                    event[field] = get_single_value(value)
                    
                event_list.append(event)
                
    return event_list
    
# Given the result of the query to find the combinations of a set of fields
# parses the result into a list of dictionaries with the combinations
def parse_aggregation_result(split_fields, result):
    import copy
    value_list = list()
    
    def auxiliary_aggregation_parser(headers, aggregations, current):
        recursed = False
        for (key, value) in aggregations.items():
            if key in headers and 'buckets' in value.keys():
                for bucket in value['buckets']:
                    new_headers = headers[:]
                    new_headers.remove(key)

                    new_current = copy.deepcopy(current)
                    new_current[key]=bucket['key']

                    auxiliary_aggregation_parser(new_headers, bucket, new_current)
                    recursed = True
        
        if not recursed:
            # TODO: Check if doc_count exists
            current['_doc_count'] = aggregations['doc_count']
            value_list.append(current)
            
    #print result
    auxiliary_aggregation_parser(split_fields, result['aggregations'], {})
        
    return value_list
    
# Creates a query that filters that filters a given query to show only results
# with the given key/value pairs
#
# Arguments:
# - query - An ElasticSearch query
# - values - A dictionary with the key/value pairs to filter the query
def create_bucket_query(query, index_size, fields, values, from_timestamp=0, to_timestamp=0):
    body = {}
    if from_timestamp == 0 and to_timestamp == 0:
        body = {
            "size": index_size,
            "query": {
                "bool": {
                    "must": [
                        query,
                    ]
                }
            }
        }
    else: 
        body = {
            "size": index_size,
            "query": {
                "bool": {
                    "must": [
                        query,
                        {
                            "range": {
                                "@timestamp": {
                                    "from": from_timestamp,
                                    "to": to_timestamp
                                }
                            }
                        },
                    ]
                }
            }
        }
                                
    for field in fields:
        field_filter = {
            "match": {},
        }
        
        field_filter['match'][field] = values[field]
        body['query']['bool']['must'].append(field_filter)
        
    return body
    
# Given a set of fields creates a query to find out all the combination
# of values for those fields
def create_aggregation_query(query, index_size, split_fields, from_timestamp=0, to_timestamp=0):
    body = {}
    if from_timestamp == 0 and to_timestamp == 0:
        body = {
            "size": 0,
            "query": {
                "bool": {
                    "must": [
                        query,
                    ]
                }
            },
            "aggregations": { }
        }
    else:
        body = {
            "size": 0,
            "query": {
                "bool": {
                    "must": [
                        query,
                        {
                            "range": {
                                "@timestamp": {
                                    "from": from_timestamp,
                                    "to": to_timestamp
                                }
                            }
                        },
                    ]
                }
            },
            "aggregations": { }
        }

    recursor = body['aggregations']
    for split_field in split_fields:
        recursor[split_field] = {
            "terms": {
                "field": split_field,
                "size": index_size,
                "shard_size": 1,
            },
            "aggregations": { }
        }
        
        recursor = recursor[split_field]['aggregations']
        
    return body
