#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy

def get_body(config, fields, event_list, split_values):
    filename = config.configparser.get('Template', 'template_file')
    template = open(filename, 'r')
    
    replacements = copy.deepcopy(split_values)
    # To add dynamic fields that aren't directly related to the data
    # do it like this:
    #replacements['___count'] = len(event_list)

    return template.read().decode('utf-8') % replacements
