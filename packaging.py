import csv
from StringIO import StringIO
import zipfile
import json    

# get_attachments should return a list in which each item is a dictionary
# with the following keys:
# - filename - the name with which the file will be attached
# - data - the raw data of the file

def get_attachments(configloader, fields, event_list, split_values):
    # CSV configs
    config = configloader.configparser
    csv_filename = config.get('CSV', 'filename').strip() % split_values
    zipcsv = json.loads(config.get('CSV', 'zip'))

    unziped_csv = StringIO()
    csvwriter = csv.DictWriter(unziped_csv, fields, delimiter=',', quotechar='"', lineterminator='\n', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writeheader()
    for hit in event_list:
        csvwriter.writerow(hit)
    
    final_csv = StringIO()
    final_csv_filename = ''

    if zipcsv:
        zip_data = StringIO()

        zfile = zipfile.ZipFile(zip_data, mode='w', compression=zipfile.ZIP_DEFLATED)
        zfile.writestr(csv_filename, unziped_csv.getvalue())
        zfile.close()

        final_csv = zip_data.getvalue()
        final_csv_filename = csv_filename + '.zip'
    else:
        final_csv = unziped_csv.getvalue()
        final_csv_filename = csv_filename

    return [{"filename": final_csv_filename, "data": final_csv}]
