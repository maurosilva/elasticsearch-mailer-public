#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2
import json

# Whois dump (CERT.PT only)
base_url = 'https://whois.rede.cert.pt/'
app_key = '930625006f1180115c59ca4434aba5aae0ba077c2d432c3be51abf2a22edf156fe7cb5b14018625bc9a1a479340468527742c9cc2afca2dee54825e619491068715563db25cbfd03702f55b6edba8c0ee323eb2ac8eeeda5a36f53db8f3602f97eea0cc22f9f08d255acb1c24487233806c179819738770e66c704e7b66f6c9b'

#response = urllib2.urlopen(base_url + app_key)
#response_text = response.read()
#entities = json.loads(response_text)

def get_headers(config, fields, values):
    return {
        "from": "mauro.silva@fccn.pt",
        "to": ['mauro.silva@fccn.pt'],
        "subject": config.configparser.get('Template', 'subject').decode('utf-8') % values,
    }
