from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

# TODO: Support SMTP/SSL

# Helper functions    
def is_iterable(obj):
    return hasattr(obj, '__iter__')

# Accepts an options dictionary with the following keys:
# * subject - mail subject
# * from - From header
# * to - To header
# * reply to - Reply-to header
# * preamble - message that appears if the MIME isn't decoded
# * text - either a string or a list of strings to include in the e-mail
# * attachments - either a dictionary or a list of dictionaries as described below
def send_mail(configloader, options):
    message = MIMEMultipart()
    message['Subject'] = options['subject']
    message['From'] = options['from']

    if 'reply to' in options:
        message['Reply-to'] = options['reply to']
    
    # If the 'to' option is a list of emails, put them comma separated
    if is_iterable(options['to']):
        message['To'] = ', '.join(options['to'])
    else:
        message['To'] = options['to']
    
    # What you see if the MIME isn't decoded
    if 'preamble' in options:
        message.preamble = options['preamble']
    else:
        message.preamble = 'Multipart MIME message.'
    
    # Attach text parts, can either be a single string or a list of strings
    if 'text' in options:
        if is_iterable(options['text']):
            for textpart in options['text']:
                print textpart
                part = MIMEText(textpart.encode('utf-8'), 'plain', 'utf-8')
                message.attach(part)
        else:
            print options['text']
            part = MIMEText(options['text'].encode('utf-8'), 'plain', 'utf-8')
            message.attach(part)
            
    # Put the attachments in the message. Accepts either a single dictionary or 
    # a list of dictionaries with the following keys:
    # * data - the attachment data
    # * filename - the name of the attached file
    if 'attachments' in options:
        if is_iterable(options['attachments']):
            for attach in options['attachments']:
                part = MIMEApplication(attach['data'])
                part.add_header('Content-Disposition', 'attachment', filename=attach['filename'])
                message.attach(part)
        else:
            part = MIMEApplication(options['attachments']['data'])
            part.add_header('Content-Disposition', 'attachment', filename=options['attachments']['filename'])
            message.attach(part)
            
    #smtp = SMTP(configloader.config['mail server']['server'], configloader.config['mail server']['port'])
    #smtp.ehlo()
    #if configloader.config['mail server']['username']:
        #smtp.login(configloader.config['mail server']['username'], configloader.config['mail server']['password'])
    #smtp.sendmail(message['From'], message['To'], message.as_string())
    
